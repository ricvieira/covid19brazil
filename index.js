/**
 * @format
 */

import { AppRegistry } from 'react-native';
import messaging from '@react-native-firebase/messaging';
import App from './src';
import { name as appName } from './app.json';
import './src/firebase';

require('react-native').unstable_enableLogBox();

// (async function checkPermission() {
//   const status = await messaging().hasPermission();
//   console.log(status);
// })();

messaging().setBackgroundMessageHandler(async (remoteMessage) => {
  // console.log('Message handled in the background!', remoteMessage);
});

AppRegistry.registerComponent(appName, () => App);
