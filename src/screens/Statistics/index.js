import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  ScrollView,
  // TouchableOpacity,
  // Animated,
  // Easing,
  // Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import AsyncStorage from '@react-native-community/async-storage';

import styles from './styles';
import api from '../../services/api';
import PieChart from '../../components/charts/PieChart';
import LineChartWithToolTip from '../../components/charts/LineChart/LineChartWithToolTip';

export default function Statistics() {
  const [covidData, setCovidData] = useState(null);
  // const [isFirstBtn, setIsFirstBtn] = useState(true);
  // const [animValue] = useState(new Animated.Value(0));

  // const WIDTH = (Dimensions.get('window').width * (90 / 100)) / 2 - 13.5;

  // const animatedStyles = {
  //   transform: [{ translateX: animValue }],
  // };

  useEffect(() => {
    getLocalData()
      .then(fetchData)
      .then(storeData)
      .catch((err) => console.log(err));
  }, []);

  // useEffect(() => {
  //   const value = WIDTH;

  //   Animated.timing(animValue, {
  //     toValue: isFirstBtn ? 1 : value,
  //     duration: 200,
  //     easing: Easing.bezier(0.75, -0.25, 0.25, 1.25),
  //     useNativeDriver: true,
  //   }).start();
  // }, [isFirstBtn]);

  async function storeData(data) {
    try {
      if (data) {
        await AsyncStorage.setItem('covidData', JSON.stringify(data));
        setCovidData(data);
      }
    } catch (err) {}
  }

  async function getLocalData() {
    try {
      const data = await AsyncStorage.getItem('covidData');
      data && setCovidData(JSON.parse(data));
    } catch (err) {}
  }

  async function fetchData() {
    try {
      const general = await api.get('/PortalGeralApi');
      const generalResult = await general.data;

      const update = await api.get('/PortalGeral');
      const updateResult = await update.data.results[0].dt_atualizacao;

      const accumulation = await api.get('/PortalCasos');
      const accumulationResult = await accumulation.data;

      const regions = await api.get('/PortalSintese');
      const regionsResult = await regions.data;

      return {
        general: generalResult,
        accumulation: accumulationResult,
        regions: regionsResult,
        update: updateResult,
      };
    } catch (err) {}
  }

  // function MenuButton({ name }) {
  //   const activeButtonStyle =
  //     (name === 'brasil') === isFirstBtn
  //       ? {
  //           color: '#1f232b',
  //           fontWeight: 'bold',
  //         }
  //       : {
  //           color: '#f1f4f7',
  //           fontWeight: 'normal',
  //         };
  //   return (
  //     <Text style={[styles.buttonTitle, activeButtonStyle]}>
  //       {name.replace(/^\w/, (c) => c.toUpperCase())}
  //     </Text>
  //   );
  // }

  // function ButtonBackground() {
  //   return (
  //     <Animated.View
  //       style={[styles.buttonBackgroundSelector, animatedStyles]}
  //     />
  //   );
  // }

  // function selectData(from) {
  //   from === 'brasil' ? setIsFirstBtn(true) : setIsFirstBtn(false);

  // }

  const formatNumber = (n) =>
    n.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1.');

  return (
    <View style={styles.container}>
      <ScrollView
        contentContainerStyle={[
          styles.container,
          { paddingHorizontal: 15, minHeight: '100%' },
        ]}>
        <View style={styles.appBar}>
          <Icon name="bell" size={20} color="#444" />
        </View>

        <Text style={{ fontSize: 24, fontWeight: 'bold', marginTop: 10 }}>
          Estatísticas
        </Text>

        {/* <View style={styles.menu}>
          <TouchableOpacity
            style={styles.menuButton}
            onPress={() => selectData('brasil')}>
            <MenuButton name="brasil" />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.menuButton}
            onPress={() => selectData('mundo')}>
            <MenuButton name="mundo" />
          </TouchableOpacity>
          <ButtonBackground />
        </View> */}

        <Text style={{ fontSize: 11, textAlign: 'right', marginTop: 10 }}>
          Última atualização: {covidData?.update}
        </Text>

        <View style={styles.panel}>
          <View style={styles.card}>
            <Text style={[styles.cardNumber, { color: '#fab822' }]}>
              {covidData && formatNumber(covidData.general.confirmados.total)}
            </Text>
            <Text style={styles.cardTitle}>Confirmados</Text>
          </View>
          <View style={styles.card}>
            <Text style={[styles.cardNumber, { color: '#fb397a' }]}>
              {covidData && formatNumber(covidData.general.obitos.total)}
            </Text>
            <Text style={styles.cardTitle}>Óbitos</Text>
          </View>
          <View style={styles.card}>
            <Text style={[styles.cardNumber, { color: '#00ae9d' }]}>
              {covidData &&
                formatNumber(covidData.general.confirmados.recuperados)}
            </Text>
            <Text style={styles.cardTitle}>Recuperados</Text>
          </View>
        </View>

        <PieChart covidData={covidData?.regions} />
        <LineChartWithToolTip covidData={covidData?.accumulation} />
      </ScrollView>
    </View>
  );
}
