import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    paddingBottom: 20,
  },

  appBar: {
    width: '100%',
    height: 50,
    justifyContent: 'flex-end',
    flexDirection: 'row',
    paddingRight: 10,
    alignItems: 'center',
  },

  logo: {
    resizeMode: 'contain',
    height: 40,
    width: 150,
  },

  menu: {
    width: '90%',
    height: 50,
    backgroundColor: '#663399f0',
    alignSelf: 'center',
    borderRadius: 50,
    marginVertical: 10,
    paddingHorizontal: 1,
    flexDirection: 'row',
  },

  menuButton: {
    width: '50%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },

  buttonTitle: {
    color: '#FFF',
    fontSize: 15,
  },

  buttonBackgroundSelector: {
    width: '50%',
    height: '96%',
    backgroundColor: '#FFF',
    borderRadius: 50,
    position: 'absolute',
    alignSelf: 'center',
    zIndex: -1,
  },

  panel: {
    height: 100,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#FFF',
    borderRadius: 4,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    marginTop: 10,
  },

  card: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },

  cardTitle: {
    color: '#1f232b',
    fontSize: 14,
    fontWeight: 'bold',
  },

  cardNumber: {
    fontSize: 25,
    fontWeight: 'bold',
  },
});

export default styles;

// primary: '#1292B4',
// white: '#FFF',
// lighter: '#F3F3F3',
// light: '#DAE1E7',
// dark: '#444',
// black: '#000',
