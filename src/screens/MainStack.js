import React from 'react';
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';

import Home from './Home';
import Search from './Search';
import Statistics from './Statistics';
import Map from './Map';
import BottomBar from '../components/BottomBar';

const Stack = createStackNavigator();
const options = {
  cardStyle: { backgroundColor: '#171A20' },
  cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
};

export default function MainStack() {
  return (
    <>
      <Stack.Navigator screenOptions={options} headerMode="none">
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Search" component={Search} />
        <Stack.Screen name="Statistics" component={Statistics} />
        <Stack.Screen name="Map" component={Map} />
      </Stack.Navigator>
      <BottomBar />
    </>
  );
}
