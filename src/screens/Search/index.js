import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  TextInput,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import firestore from '@react-native-firebase/firestore';
import messaging from '@react-native-firebase/messaging';
import api from '../../services/api';
import styles from './styles';

export default function Search() {
  const [search, setSearch] = useState();
  const [searched, setSearched] = useState([]);
  const [results, setResults] = useState([]);

  useEffect(() => {
    fetchData();
  }, []);

  useEffect(() => {
    if (search) {
      const searchResult = results.filter((item) =>
        item.nome.match(new RegExp(`${search}`, 'ig')),
      );
      setSearched(searchResult);
    }
  }, [search, results]);

  async function trackCity(track) {
    let token = await messaging().getToken();
    try {
      await firestore()
        .collection('trackedCities')
        .doc(track)
        .update({
          tokens: firestore.FieldValue.arrayUnion(token),
        });
    } catch (error) {
      if (error.code === 'firestore/not-found') {
        await firestore()
          .collection('trackedCities')
          .doc(track)
          .set({
            tokens: [token],
          });
      } else {
        console.log(error);
      }
    }
  }

  async function fetchData() {
    try {
      const response = await api.get('/PortalMunicipio');
      const data = await response.data;

      setResults(data);
    } catch (err) {
      console.log(err);
    }
  }

  function RenderSearchItem({ item }) {
    return (
      <TouchableOpacity onLongPress={() => trackCity(item.nome)}>
        <View style={styles.item}>
          <Text>{item.nome}</Text>
        </View>
      </TouchableOpacity>
    );
  }

  function _keyExtractor({ cod }) {
    return `${cod}`;
  }

  return (
    <View style={styles.container}>
      <View style={styles.appBar}>
        <Icon name="bell" size={20} color="#444" />
      </View>

      <Text style={{ fontSize: 24, fontWeight: 'bold', marginTop: 10 }}>
        Pesquisa
      </Text>

      <TextInput
        style={styles.search}
        placeholder="Search"
        placeholderTextColor="#999"
        value={search}
        onChangeText={setSearch}
      />
      <FlatList
        style={styles.searchContent}
        data={searched?.slice(0, 99)}
        initialNumToRender={30}
        renderItem={RenderSearchItem}
        keyExtractor={_keyExtractor}
      />
    </View>
  );
}
