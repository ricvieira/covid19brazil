import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    height: '100%',
    paddingHorizontal: 15,
  },

  appBar: {
    width: '100%',
    height: 50,
    justifyContent: 'flex-end',
    flexDirection: 'row',
    paddingRight: 10,
    alignItems: 'center',
  },

  logo: {
    resizeMode: 'contain',
    height: 40,
    width: 150,
  },

  menu: {
    width: '90%',
    height: 50,
    backgroundColor: '#663399f0',
    alignSelf: 'center',
    borderRadius: 50,
    marginVertical: 10,
    paddingHorizontal: 1,
    flexDirection: 'row',
  },

  menuButton: {
    width: '50%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },

  search: {
    width: '100%',
    height: 46,
    backgroundColor: '#F3F3F3',
    borderRadius: 3,
    marginBottom: 20,
    paddingHorizontal: 15,
  },

  item: {
    width: '100%',
    padding: 15,
    backgroundColor: '#DAE1E7',
    borderRadius: 3,
    marginBottom: 5,
  },
});

export default styles;

// primary: '#1292B4',
// white: '#FFF',
// lighter: '#F3F3F3',
// light: '#DAE1E7',
// dark: '#444',
// black: '#000',
