import React, { useEffect, useState } from 'react';
import { View, Text, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import messaging from '@react-native-firebase/messaging';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import styles from './styles';

export default function Home() {
  const [userId, setUserId] = useState(null);

  useEffect(() => {
    messaging().getInitialNotification();

    auth()
      .signInAnonymously()
      .then((a) => setUserId(a.user.uid))
      .catch((e) => console.log('[auth] ', e.message));
  }, []);

  useEffect(() => {
    if (!userId) {
      return;
    }

    getToken();
    return messaging().onTokenRefresh((token) => {
      saveTokenToDatabase(token);
    });
  }, [userId]);

  async function getToken() {
    try {
      const token = await messaging().getToken();
      await saveTokenToDatabase(token);
    } catch (error) {
      console.log(error);
    }
  }

  async function saveTokenToDatabase(token) {
    try {
      await firestore()
        .collection('users')
        .doc(userId)
        .update({
          tokens: firestore.FieldValue.arrayUnion(token),
        });
    } catch (error) {
      if (error.code === 'firestore/not-found') {
        await firestore()
          .collection('users')
          .doc(userId)
          .set({
            tokens: [token],
          });
      } else {
        console.log(error);
      }
    }
  }

  return (
    <View style={styles.container}>
      <ScrollView
        contentContainerStyle={[
          styles.container,
          { paddingHorizontal: 15, minHeight: '100%' },
        ]}>
        <View style={styles.appBar}>
          <Icon name="bell" size={20} color="#444" />
        </View>

        <Text style={{ fontSize: 24, fontWeight: 'bold', marginTop: 10 }}>
          Home
        </Text>
      </ScrollView>
    </View>
  );
}
