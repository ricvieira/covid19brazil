import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import MapboxGL from '@react-native-mapbox-gl/maps';
import axios from 'axios';

import styles from './styles';
import BackButton from '../../components/BackButton';

MapboxGL.setAccessToken(
  'pk.eyJ1IjoicmljdmllaXJhIiwiYSI6ImNrN3NoMnFwdzBmazAzbG5xZXIyMjQ2YncifQ.h535ZzgHLmrs8CqHnfcp3Q',
);

export default function Map({ navigation, route }) {
  const [cities, setCities] = useState(null);

  useEffect(() => {
    // MapboxGL.setTelemetryEnabled(false);

    (async function getCities() {
      try {
        const url =
          'https://gist.githubusercontent.com/vieiraricardo/1bf770c42f6607d794127a7e0a9469ab/raw/fed04a779ce49e93a27476b520351d74e9cf1135/municipios.json';
        const response = await axios.get(url);
        const data = await response.data;

        setCities(data);
      } catch (error) {
        console.log(error);
      }
    })();
  }, []);

  const AnnotationContent = ({ title }) => (
    <TouchableOpacity
      style={{
        backgroundColor: 'red',
        width: 5,
        height: 5,
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
      }}
    />
  );

  return (
    <View style={[styles.container, { justifyContent: 'center' }]}>
      <BackButton color="#444" onPress={() => navigation.goBack()} />
      <MapboxGL.MapView
        logoEnabled={false}
        attributionPosition={{ bottom: 20, right: 20 }}
        style={{ flex: 1 }}
        styleURL={MapboxGL.StyleURL.Light}>
        <MapboxGL.Camera
          zoomLevel={3}
          centerCoordinate={[-52.19873400742284, -11.527930265771431]}
        />

        {cities?.slice(0, 100).map((city) => (
          <MapboxGL.PointAnnotation
            key={city.codigo_ibge}
            coordinate={[city.longitude, city.latitude]}
            id="pt-ann">
            <AnnotationContent title={'this is a point annotation'} />
          </MapboxGL.PointAnnotation>
        ))}
      </MapboxGL.MapView>
      <View style={styles.card} />
    </View>
  );
}

// longitude e latitude
