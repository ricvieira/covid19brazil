import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },

  card: {
    width: '96%',
    height: '30%',
    backgroundColor: '#F5F5F5',
    borderRadius: 30,
    position: 'absolute',
    bottom: 70,
    alignSelf: 'center',
  },
});

export default styles;
