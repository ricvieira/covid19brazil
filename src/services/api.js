import axios from 'axios';

const api = axios.create({
  baseURL: 'https://xx9p7hp1p7.execute-api.us-east-1.amazonaws.com/prod',
  headers: {
    'Content-Type': 'application/json',
    'X-Parse-Application-Id': 'unAFkcaNDeXajurGB7LChj8SgQYS2ptm',
    'User-Agent':
      'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
  },
});

export default api;
