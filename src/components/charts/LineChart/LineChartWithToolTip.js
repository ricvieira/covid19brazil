import React, { useEffect, useState } from 'react';
import { View, Text } from 'react-native';
import { Grid, LineChart, YAxis } from 'react-native-svg-charts';
import { Circle } from 'react-native-svg';
import styles from './styles';

export default function LineChartWithToolTip({ covidData }) {
  const [data, setData] = useState(null);

  useEffect(() => {
    const cases = covidData?.dias.map((day) => day.casosNovos);
    cases && setData(cases);
  }, [covidData]);

  function getLargest() {
    let max = 0;
    data.forEach((num) => {
      num > max && (max = num);
    });
    return max;
  }

  const Decorator = ({ x, y }) => {
    return data.map((value, index) => (
      <Circle
        key={index}
        cx={x(index)}
        cy={y(value)}
        r={1}
        stroke={'rgb(134, 65, 244)'}
        fill={'white'}
      />
    ));
  };

  return (
    <View style={styles.dailyUpdates}>
      <Text style={styles.chartDescription}>
        Casos novos de COVID-19 por data de notificação
      </Text>

      {data && (
        <View>
          <LineChart
            style={{
              height: 200,
              width: '90%',
              alignSelf: 'flex-end',
              marginTop: 10,
              marginRight: 10,
              borderLeftWidth: 1,
              borderLeftColor: '#ccc',
            }}
            data={data}
            numberOfTicks={10}
            svg={{
              stroke: 'rgb(134, 65, 244)',
              strokeWidth: 2,
            }}
            animate={true}>
            <Grid />
            <Decorator />
          </LineChart>
          <YAxis
            style={{
              height: '100%',
              position: 'absolute',
              top: 2,
              bottom: 0,
              left: 5,
            }}
            numberOfTicks={10}
            data={[0, getLargest()]}
            contentInset={{ top: 10, bottom: 5 }}
            svg={{
              fontSize: 7,
              fill: 'black',
              stroke: 'black',
              strokeWidth: 0.1,
            }}
          />
        </View>
      )}
    </View>
  );
}
