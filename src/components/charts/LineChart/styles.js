import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  dailyUpdates: {
    width: '100%',
    height: 300,
    marginBottom: 50,
    backgroundColor: '#FFF',
    borderRadius: 4,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
  },

  chartDescription: {
    fontSize: 16,
    fontWeight: 'bold',
    paddingVertical: 10,
    paddingHorizontal: 10,
  },

  chartButtons: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingHorizontal: '10%',
    marginTop: 20,
  },

  chartButton: {
    paddingHorizontal: 20,
    paddingVertical: 8,
    borderRadius: 4,
    backgroundColor: '#ffffff02',
  },

  chartButtonText: {
    color: '#1f232b',
    fontSize: 12,
  },

  subtitles: {
    // width: '100%',
    padding: 10,
    justifyContent: 'space-around',
    flexDirection: 'row',
  },

  subtitle: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  subtitleMarker: {
    width: 10,
    height: 10,
    borderRadius: 2,
    marginRight: 5,
  },

  subtitleTitle: {
    fontSize: 12,
    color: '#1f232b',
  },
});

export default styles;
