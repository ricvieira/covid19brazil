import React from 'react';
import { View, Text } from 'react-native';
import { Grid, LineChart, YAxis } from 'react-native-svg-charts';

import styles from './styles';

export default function LineCharts({ covidData }) {
  return (
    <View style={styles.dailyUpdates}>
      <View style={styles.chartButtons}>
        <View style={styles.chartButton}>
          <Text style={styles.chartButtonText}>3 dias</Text>
        </View>
        <View style={styles.chartButton}>
          <Text style={styles.chartButtonText}>15 dias</Text>
        </View>
        <View style={styles.chartButton}>
          <Text style={styles.chartButtonText}>30 dias</Text>
        </View>
      </View>

      {covidData && (
        <View>
          <LineChart
            style={{
              height: 200,
              width: '90%',
              alignSelf: 'flex-end',
              marginRight: 10,
              marginTop: 10,
              borderLeftWidth: 1,
              borderLeftColor: '#ccc',
            }}
            data={[
              {
                data: covidData.map((v) => v.qtd_confirmado),
                svg: { stroke: '#0088ff', strokeWidth: 1.5 },
              },
              {
                data: covidData.map((v) => v.qtd_obito),
                svg: { stroke: '#fb397a', strokeWidth: 1.5 },
              },
            ]}>
            <Grid />
          </LineChart>

          <YAxis
            style={{
              height: '100%',
              position: 'absolute',
              top: 0,
              bottom: 0,
              left: 5,
            }}
            data={[
              0,
              Math.ceil(covidData[covidData.length - 1].qtd_confirmado),
            ]}
            formatLabel={(item) => (item === 0 ? 0 : `${item / 1000}k`)}
            contentInset={{ top: 10, bottom: 5 }}
            svg={{
              fontSize: 7,
              fill: 'black',
              stroke: 'black',
              strokeWidth: 0.1,
            }}
          />
        </View>
      )}

      <View style={styles.subtitles}>
        <View style={styles.subtitle}>
          <View
            style={[styles.subtitleMarker, { backgroundColor: '#0088ff' }]}
          />
          <Text style={styles.subtitleTitle}>Confirmados</Text>
        </View>
        <View style={styles.subtitle}>
          <View
            style={[styles.subtitleMarker, { backgroundColor: '#fb397a' }]}
          />
          <Text style={styles.subtitleTitle}>Óbitos</Text>
        </View>
      </View>
    </View>
  );
}
