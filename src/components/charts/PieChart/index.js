import React, { useEffect, useState } from 'react';
import {
  View,
  Text,
  TouchableNativeFeedback,
  Animated,
  Easing,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { PieChart } from 'react-native-svg-charts';

import styles from './styles';

export default function PieCharts({ covidData }) {
  const navigation = useNavigation();
  const [data, setData] = useState([]);
  const AnimatedPieChart = Animated.createAnimatedComponent(PieChart);
  const colors = ['#fb397a', '#1fbb87', '#5d78ff', '#fab822', '#0088ff'];
  const animValue = new Animated.Value(0);

  let endAngle = Animated.multiply(animValue, Math.PI);

  useEffect(() => {
    if (!covidData) {
      return;
    }

    covidData.shift();
    const newData = covidData;

    const _data = newData.reduce((acc, cur, i) => {
      const item = { ...cur, key: cur._id, svg: { fill: colors[i] } };
      return [...acc, item];
    }, []);

    setData(_data);
  }, [covidData]);

  useEffect(() => {
    if (data) {
      animate();
    }

    function animate() {
      Animated.timing(animValue, {
        toValue: 2,
        duration: 700,
        easing: Easing.inOut(Easing.quad),
        useNativeDriver: false,
      }).start();
    }
  }, [data, animValue]);

  return (
    <View style={styles.container}>
      <Text style={styles.chartDescription}>Casos COVID-19 por região</Text>

      <View style={styles.chartContainer}>
        <AnimatedPieChart
          style={styles.chart}
          valueAccessor={({ item }) => item.casosAcumulado}
          data={data}
          spacing={0}
          outerRadius={'95%'}
          animate={false}
          innerRadius={'65%'}
          padAngle={0.03}
          endAngle={endAngle}
        />
        <View style={styles.subtitles}>
          {data?.map((item, i) => (
            <TouchableNativeFeedback
              key={i}
              onPress={() => navigation.navigate('Map')}>
              <View style={styles.sub}>
                <View style={styles.subtitle}>
                  <View
                    style={[
                      styles.subtitleMarker,
                      { backgroundColor: colors[i] },
                    ]}
                  />
                  <Text style={styles.subText}>{item._id}</Text>
                </View>
                <Text style={styles.subNumber}>{item.casosAcumulado}</Text>
              </View>
            </TouchableNativeFeedback>
          ))}
        </View>
      </View>

      <Text style={styles.chartInfoNote}>
        Fonte: https://covid.saude.gov.br/
      </Text>
    </View>
  );
}
