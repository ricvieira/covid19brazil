import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    width: '100%',
    marginTop: 20,
    marginBottom: 20,
    backgroundColor: '#FFF',
    borderRadius: 4,
    padding: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
  },

  chartInfoNote: {
    marginTop: 15,
    fontSize: 10,
    color: '#1f232b',
    borderTopColor: '#DDD',
    borderTopWidth: StyleSheet.hairlineWidth,
    paddingTop: 10,
  },

  chartContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
  },

  chartDescription: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 20,
  },

  chart: {
    width: 180,
    height: 180,
  },

  chartInfo: {
    width: '100%',
    paddingTop: 20,
    flexDirection: 'row',
    justifyContent: 'center',
  },

  subtitles: {
    justifyContent: 'flex-end',
    marginLeft: '10%',
  },

  sub: {
    padding: 2,
  },

  subNumber: {
    fontSize: 14,
    color: '#1f232b',
    fontWeight: 'bold',
  },

  subText: {
    fontSize: 12,
    color: '#1f232b',
    borderRadius: 3,
    alignSelf: 'flex-start',
    paddingHorizontal: 3,
    lineHeight: 15,
  },

  subtitle: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  subtitleMarker: {
    width: 10,
    height: 10,
    borderRadius: 2,
  },
});

export default styles;
