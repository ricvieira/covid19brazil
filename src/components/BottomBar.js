import React from 'react';
import { View, StyleSheet, TouchableNativeFeedback } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import { useNavigationState } from '@react-navigation/native';
import { navigate } from '../Rootnavigation';

const icons = ['home', 'trending-up', 'search'];

export default function BottomBar() {
  const routeIndex = useNavigationState((state) =>
    state.routes[0].state ? state.routes[0].state.index : 0,
  );

  function _navigate(name) {
    if (name === 'home') {
      return navigate('Home');
    } else if (name === 'trending-up') {
      return navigate('Statistics');
    } else if (name === 'search') {
      return navigate('Search');
    }
  }

  function activeButton(index) {
    return routeIndex === index ? '#777' : '#ccc';
  }

  return (
    <View style={styles.container}>
      {icons.map((name, i) => (
        <TouchableNativeFeedback key={name} onPress={() => _navigate(name)}>
          <View style={styles.button}>
            <Icon name={name} size={28} color={activeButton(i)} />
          </View>
        </TouchableNativeFeedback>
      ))}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: '#F5F5F5',
    width: '95%',
    height: 56,
    elevation: 3,
    position: 'absolute',
    bottom: 10,
    paddingHorizontal: 40,
    alignSelf: 'center',
    borderRadius: 50,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  button: {
    padding: 14.5,
  },
});
