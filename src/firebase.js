import firebase from '@react-native-firebase/app';
import '@react-native-firebase/database';

var config = {
  apiKey: 'AIzaSyBwftfjzrSdjElNMIWRUj8FGyghpl95aLk',
  authDomain: 'covid19brazil-e9765.firebaseapp.com',
  databaseURL: 'https://covid19brazil-e9765.firebaseio.com',
  projectId: 'covid19brazil-e9765',
  storageBucket: 'covid19brazil-e9765.appspot.com',
  messagingSenderId: '227836170754',
  appId: '1:227836170754:web:5567da9c8f8fdabaa48d1b',
};

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

export const database = firebase.database();
